function problem1(inventory, req_id) {
    if (typeof (inventory) === "object" && inventory != null && req_id != null) {
        if (Array.isArray(inventory) != true) {
            return [];
        }
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].id == req_id) {
                return inventory[index];
            }
        }
    }
    return [];
}

module.exports = problem1;

