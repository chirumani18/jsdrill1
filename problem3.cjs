const inventory = require('./inventory.cjs')
function problem3(inventory) {
    const model = [];
    for (let index = 0; index < inventory.length; index++) {
        model[index] = inventory[index].car_model;
    }
    model.sort();
    const sort_inventory = [];
    for (let index = 0; index < model.length; index++) {
        for (let index2 = 0; index2 < inventory.length; index2++) {
            if (inventory[index2].car_model == model[index]) {
                sort_inventory[index] = inventory[index2];
            }
        }
    }
    return sort_inventory;
}
module.exports = problem3;