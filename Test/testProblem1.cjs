const inventory = require('../inventory.cjs')
const result = require('../problem1.cjs')
let car_details = result(inventory, 33);

console.log("Car 33 is a " + car_details.car_year + " " + car_details.car_make + " " + car_details.car_model);