const inventory = require('./inventory.cjs');
const result = require('./problem2.cjs');

let res = result(inventory);
console.log("Last car is a " + res.car_make + " " + res.car_model);