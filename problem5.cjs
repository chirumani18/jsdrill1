const old_cars = []
function problem5(car_years) {
    let index2 = 0;
    for (let index = 0; index < car_years.length; index++) {
        if (car_years[index] < 2000) {
            old_cars[index2] = car_years[index];
            index2++;
        }
    }
    return old_cars;
}

module.exports = problem5;