function problem4(inventory) {
    const car_years = [];
    for (let index = 0; index < inventory.length; index++) {
        car_years[index] = inventory[index].car_year;
    }
    return car_years;
}

module.exports = problem4;